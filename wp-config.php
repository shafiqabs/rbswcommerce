<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'woocommerce' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '1' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+6sMc%4 GJJ%G7;sou#E(Z|2}f^~L;=Uv$={.|>6eYSC9C^uI{T{o2 #Xo)}Js%?' );
define( 'SECURE_AUTH_KEY',  '6KVn-:WdZCbzKuVBust@y|3RdFE`YPR9f>aOX][O`,*uO}t~-Uawlw&i;~kS@(bG' );
define( 'LOGGED_IN_KEY',    '1`Uqk{DaO8pX00;,8x Bs?-&s;.65~%#{iZ1)y4H^ wt$yC2^a5Ub/=#f7FXhJS|' );
define( 'NONCE_KEY',        '#<LL7X|EBG)`R(Z{R]~F07]~Bvv_)~wdadLXwd,Shb<K^/Xw1$>mPL^os4|uRS,G' );
define( 'AUTH_SALT',        'W3c<-o,p>x/<|hav]ngF(TYDtk4GF#wRy)r51iZ7-obe]9bXk;)c&r;he7y`;.<W' );
define( 'SECURE_AUTH_SALT', '<_bkg ^b5PXx^V?^k%7~3wZ#4a<5?651*j1kx*dPOJC@/~E!7(k~u42KAv&b{!IJ' );
define( 'LOGGED_IN_SALT',   'ymi`>n3Y[*+kc.jO[RJILvNRyp&@|U_N-.4K76_#6AIgZ@ePs$^3TIhYEHfw(7W[' );
define( 'NONCE_SALT',       'Cq:q9?q_[jHs*QG;A,yh?[3ZddALr2Xt!alU =xKBghxgm]2$:-.n*:k66nYH+14' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
define('FS_METHOD', 'direct');